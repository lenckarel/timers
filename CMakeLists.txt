cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project("timers")

add_executable(timers timers.c tm/tm.c)
target_link_libraries(timers m)
