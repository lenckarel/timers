#include <stdio.h>
#include <math.h>

/* Comment this to prevent time measurements */
#define TM_MEASTIME
#include "tm/tm.h"

/* Used simply as numbering of the events, what matters is the TMEvent struct */
enum {
   TME_EVENT_SQRT = 0,
   TME_EVENT_POW,
   TME_EVENT_ALL
};


int main()
{
    int i, num_rep = 100000;

    /* Can be defined anywhere... */
    TMEvent tmr_events [] = {
       {"sqrt", TM_INIT_STATE},
       {"power ", TM_INIT_STATE},
       {"total", TM_INIT_STATE},
       {"", TM_INIT_STATE}
    };

    TMEV_TIC(tmr_events,TME_EVENT_ALL);
    for (i = 0; i < num_rep; ++i) {
        TMEV_TIC(tmr_events,TME_EVENT_SQRT);
        sqrt(2.);
        TMEV_TOC(tmr_events,TME_EVENT_SQRT);
    }

    for (i = 0; i < num_rep; ++i) {
        TMEV_TIC(tmr_events,TME_EVENT_POW);
        pow(2., 1.46);
        TMEV_TOC(tmr_events,TME_EVENT_POW);
    }
    TMEV_TOC(tmr_events,TME_EVENT_ALL);

    printf("Event Name\tNum meas.\tTotal time[s]\tAvg time[s]\tMeas stdev [s]\n");
    tm_events_print(tmr_events);
    /* Eventually can be exported to csv to with tm_events_savetocsv,
     * Also supports export to matlab structure when mex.h included.
     */
    return 0;
}
