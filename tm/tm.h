#ifndef TM_H
#define TM_H
/** Time Machine
 *
 * Simple system for measuring statistics of exucution time
 * of parts of a code.
 */


/* TODO Allow to use tick only every Nth measurement
 * (there is a limited granularity of the time measurements,
 * in fast processes the STD is bigger than average...) */

/* Running variance code: http://www.johndcook.com/standard_deviation.html */

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "host.h"

/* Fills in an empty structure of time statistics */
#define TM_INIT_STATE {0.,0,0.,0.,0.,0.,0.}

#ifdef TM_MEASTIME
   /* Single event triggers */
   #define TMEV_TIC(EVNTS,EVNT_NUM) do {tm_tic(&EVNTS[EVNT_NUM].state);} while (0)
   #define TMEV_TOC(EVNTS,EVNT_NUM) do {tm_toc(&EVNTS[EVNT_NUM].state);} while (0)

   /* Two events triggers */
   #define TMEV_TIC2(EVNTS,EVNT1_NUM,EVNT2_NUM) do {\
      tm_tic(&EVNTS[EVNT1_NUM].state);\
      tm_tic(&EVNTS[EVNT2_NUM].state);\
   } while (0)
   #define TMEV_TOC2(EVNTS,EVNT1_NUM,EVNT2_NUM) do {\
      tm_toc(&EVNTS[EVNT1_NUM].state);\
      tm_toc(&EVNTS[EVNT2_NUM].state);\
   } while (0)
#else
   #define TMEV_TIC(EVNTS,EVNT_NUM)
   #define TMEV_TOC(EVNTS,EVNT_NUM)

   #define TMEV_TIC2(EVNTS,EVNT1_NUM,EVNT2_NUM)
   #define TMEV_TOC2(EVNTS,EVNT1_NUM,EVNT2_NUM)
#endif

/* External code for measuring the exection time */
VL_EXPORT double getRealTime();

/* Time statistics */
typedef struct _TMState {
  double start_time;   /* Time of the last TIC */
  int    n;            /* Number of collected measurements*/
  double oldM;
  double newM;
  double oldS;
  double newS;
  double total;        /* Total execution time of that part */
} TMState;

/* Named time event */
typedef struct _TMEvent
{
  const char *name ;
  TMState state;
} TMEvent;

VL_INLINE void    tm_reset(TMState* obj);

VL_INLINE void    tm_tic(TMState * obj);           /* Starts the timer */
VL_INLINE double  tm_toc(TMState * obj);           /* Takes a sample and resets the timer */

VL_INLINE double  tm_elapsed(TMState const * obj); /* Returns elapsed time */
VL_INLINE int     tm_nmeas(TMState const * obj);   /* Returns num. of samples */
VL_INLINE double  tm_avg(TMState const * obj);     /* Returns avg. time */
VL_INLINE double  tm_var(TMState const * obj);     /* Returns variance of time measurements */
VL_INLINE double  tm_std(TMState const * obj);     /* Returns std. dev. of time meas. */
VL_INLINE double  tm_total(TMState const * obj);   /* Returns total measured time */

/* Operations with arrays of named events */
VL_EXPORT void tm_events_reset(TMEvent *events);
VL_EXPORT void tm_events_print(TMEvent const *events);
VL_EXPORT void tm_events_savetocsv(TMEvent const *events, const char *fileName);

#ifdef mex_typedefs_h
void tm_events_matlab_export(TMEvent const *events, mxArray** out)
{
   int nevents = 0;
   while(strlen(events[nevents].name) > 0) {
      nevents++;
   }

   const char *tm_stats_field_names[] =
   {
      "name", "nmeas", "total", "avg", "std"
   };

   *out = mxCreateStructMatrix(1, nevents, sizeof(tm_stats_field_names)/sizeof(const char*), tm_stats_field_names);

   int eid = 0;
   for (eid = 0; eid < nevents; eid++) {
      mxSetField(*out, eid, "name", mxCreateString(events[eid].name));
      mxSetField(*out, eid, "nmeas", mxCreateDoubleScalar((double)tm_nmeas(&events[eid].state)));
      mxSetField(*out, eid, "total", mxCreateDoubleScalar(tm_total(&events[eid].state)));
      mxSetField(*out, eid, "avg", mxCreateDoubleScalar(tm_avg(&events[eid].state)));
      mxSetField(*out, eid, "std", mxCreateDoubleScalar(tm_std(&events[eid].state)));
   }
}
#endif
/* -------------------------------------------------------------------
 *                                     Inline functions implementation
 * ---------------------------------------------------------------- */

VL_INLINE void tm_reset(TMState* obj)
{
   obj->n = 0;
   obj->start_time = 0.;
}

VL_INLINE double tm_elapsed(TMState const * obj)
{
   if (obj->start_time == 0.) {
      fprintf(stderr,"ERROR, TIC must be called before TOC.\n"); fflush(stderr);
      return -1.;
   }
   return getRealTime() - obj->start_time;
}

VL_INLINE void tm_tic(TMState* obj)
{
   if (obj->start_time != 0.) {
      fprintf(stderr,"WARNING, TIC called on a running timer.\n"); fflush(stderr);
   }
   obj->start_time = getRealTime();
}

VL_INLINE double tm_toc(TMState* obj)
{
   double elapsed = tm_elapsed(obj);
   obj->start_time = 0.;

   obj->n++;

   /* See Knuth TAOCP vol 2, 3rd edition, page 232 */
   if (obj->n == 1) {
       obj->oldM = obj->newM = elapsed;
       obj->oldS = 0.0;
       obj->total = elapsed;
   } else {
       obj->newM = obj->oldM + (elapsed - obj->oldM)/obj->n;
       obj->newS = obj->oldS + (elapsed - obj->oldM)*(elapsed - obj->newM);
       /* set up for next iteration */
       obj->oldM = obj->newM;
       obj->oldS = obj->newS;
       obj->total += elapsed;
   }
   return elapsed;
}

VL_INLINE int tm_nmeas(TMState const * obj)
{
   return obj->n;
}

VL_INLINE double tm_avg(TMState const * obj)
{
   return (obj->n > 0) ? obj->newM : 0.0;
}

VL_INLINE double tm_var(TMState const * obj)
{
   return ( (obj->n > 1) ? obj->newS/(obj->n - 1) : 0.0 );
}

VL_INLINE double tm_std(TMState const * obj)
{
   return sqrt(tm_var(obj));
}

VL_INLINE double tm_total(TMState const * obj)
{
   return obj->total;
}

#endif /* TM_H */
